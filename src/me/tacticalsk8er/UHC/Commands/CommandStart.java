package me.tacticalsk8er.UHC.Commands;

import java.io.File;
import java.util.Random;

import me.tacticalsk8er.UHC.GameEvents;
import me.tacticalsk8er.UHC.Main;
import me.tacticalsk8er.UHC.SqlHelper;
import me.tacticalsk8er.UHC.WorldData;
import net.minecraft.server.v1_7_R4.BlockAir;
import net.minecraft.server.v1_7_R4.Blocks;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/*
 * Starts a UHC game.
 * This class requires:
 * 	- Main Class (JavaPlugin)
 * 	- CommandSender (To send important messages)
 *  - Command Args (To get other info for future features)
 */

public class CommandStart {

	
	static Plugin plugin;
	
	public CommandStart(Main instance) {
		plugin = instance;
	}
	
	
	public CommandStart(Main plugin2, CommandSender sender) {
		
		plugin = Main.plugin;
		int Raduis = plugin.getConfig().getInt("GameSettings.Raduis");
		sender.sendMessage("Creating New World");
		WorldCreator UHCreate = new WorldCreator("UHC");
		Random r = new Random();
		Long l = r.nextLong();
		UHCreate.seed(l);
		final World UHC = UHCreate.createWorld();


		UHC.getEntities().clear();
		
		UHC.setPVP(true);
		UHC.setDifficulty(Difficulty.PEACEFUL);
		UHC.setGameRuleValue("naturalRegeneration", "false");
		UHC.setGameRuleValue("keepInventory", "true");
		UHC.setTime(0);
		
		// Teleport players to new world
		sender.sendMessage("Teleporting Players");
		StringBuilder players = new StringBuilder();
		for (Player p : plugin.getServer().getOnlinePlayers()) {
			GameEvents.spectators.removePlayer(p);
			Location tele = UHC.getSpawnLocation();
			p.teleport(tele);
			p.setBedSpawnLocation(tele);
			players.append(p.getName() + " ");
			p.sendMessage(ChatColor.GOLD + "[UHC]" + ChatColor.GREEN + " Game Starting soon, prepare for some lag.");
			Main.PlayerCount++;

		}

		for (int i = 0; i <= Raduis; i++) {
			for (int y = 0; y <= 128; y++) {
				Block block = UHC.getBlockAt(Raduis - i, y, Raduis);
				Block block2 = UHC.getBlockAt(Raduis, y, Raduis - i);
				block.setType(Material.BEDROCK);
				block2.setType(Material.BEDROCK);
			}
			for (int y = 0; y <= 128; y++) {
				Block block = UHC.getBlockAt(-Raduis + i, y, Raduis);
				Block block2 = UHC.getBlockAt(-Raduis, y, Raduis - i);
				block.setType(Material.BEDROCK);
				block2.setType(Material.BEDROCK);
			}
			for (int y = 0; y <= 128; y++) {
				Block block = UHC.getBlockAt(Raduis - i, y, -Raduis);
				Block block2 = UHC.getBlockAt(Raduis, y, -Raduis + i);
				block.setType(Material.BEDROCK);
				block2.setType(Material.BEDROCK);
			}
			for (int y = 0; y <= 128; y++) {
				Block block = UHC.getBlockAt(-Raduis + i, y, -Raduis);
				Block block2 = UHC.getBlockAt(-Raduis, y, -Raduis + i);
				block.setType(Material.BEDROCK);
				block2.setType(Material.BEDROCK);
			}
		}
		SqlHelper.updateInfo();
		sender.sendMessage("Spreading Players");
		plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), "spreadplayers 0 0 " + plugin.getConfig().getInt("GameSettings.SpreadDistance") + " " + plugin.getConfig().getInt("GameSettings.Raduis") + " true " + players);
		int id = Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			int seconds = 5;
			int timeschecked = 0;
			public void run() {
				CheckPlayers();
	}

},200);

		plugin.getServer().broadcastMessage(ChatColor.GOLD + "[UHC]" + ChatColor.GREEN + "Done!");
		// Timed Start
		Main.GameCountDown = true;
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
			int seconds = 30;
			public void run() {
				if (seconds > 5) {
					seconds--;
				} else if (seconds == 0) {
					Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "[UHC]" + ChatColor.GREEN + "==========| " + ChatColor.GOLD + "UHC" + ChatColor.GREEN + " |========");
					Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "[UHC]" + ChatColor.GREEN + "Survive as long as possible!");
					Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "[UHC]" + ChatColor.GOLD + "Players: " + ChatColor.GREEN + Bukkit.getServer().getOnlinePlayers().length + ChatColor.DARK_GREEN + "/24");
					Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "[UHC]" + ChatColor.GREEN + "Good Luck. Have Fun!");
					Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "[UHC]" + ChatColor.GREEN + "==========| " + ChatColor.GOLD + "UHC" + ChatColor.GREEN + " |========");
					
					for (Player p : Bukkit.getServer().getOnlinePlayers()) {
						p.playSound(p.getLocation(), Sound.NOTE_PIANO, 1f, 1f);
						p.setFoodLevel(20);
						p.setHealth(20);
						p.setGameMode(GameMode.SURVIVAL);
						p.getInventory().clear();
						UHC.setDifficulty(Difficulty.HARD);
					}
					seconds--;
					Main.GameCountDown = false;
					Main.GameStarted = true;
					WorldData.StartClosing();
					UHC.setGameRuleValue("naturalRegeneration", "false");
				}else if (seconds > 0){
					Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "[UHC]" + ChatColor.GREEN + seconds + " second(s) remaining!");
					for (Player p : Bukkit.getServer().getOnlinePlayers()) {
						p.playSound(p.getLocation(), Sound.NOTE_PIANO, 1f, .5f);
					}
					seconds--;
				}else if (seconds==30){
					
					
			}
			}

		}, 600L, 20L);

	}
public void CheckPlayers(){
        
		String p2 = "";
		for (Player p : Bukkit.getServer().getOnlinePlayers()) {
			Location loc = p.getLocation();
		loc = loc.add(0, -1, 0);
		Block block = loc.getBlock();
		block.setType(Material.STONE);
		Biome a = loc.getWorld().getBiome(loc.getBlockX(), loc.getBlockZ());
		Location loc2 = p.getLocation();
		loc2 = loc.add(0,1,0);
        Location loc3 = p.getLocation();
        loc3 = loc.add(0,2,0);
		if(!(loc2.getBlock().getTypeId() == 0)){
			System.out.println("Player: " + p.getDisplayName() +" failed check: ENVCHECK_HEAD_BLOCKED_BY+" +loc2.getBlock().toString());
			System.out.println("Player: " + p.getDisplayName() +" failed check: ENVCHECK_HEAD_BLOCKED_BY+" +loc2.getBlock().getTypeId());
            p2 = p2 + p.getName() + " ";
            while((!(loc2.getBlock().getTypeId() == 0))){
            	p.teleport(loc2);
            	loc2 = p.getLocation();
            	loc2.add(0,1,0);
            }
            World w = p.getWorld();
            Location l = p.getLocation();
            l.add(0,-1,0);
            Block b = l.getBlock();
            b.setType(Material.BEDROCK);
            System.out.println("Player: " + p.getDisplayName() +" is done and ready");
		}else{
			System.out.println("Player: " + p.getDisplayName() +" Didn't fail any checks. NEXT!");
		}
		}
	
	}
}
