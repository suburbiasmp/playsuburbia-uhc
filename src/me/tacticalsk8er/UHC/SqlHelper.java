package me.tacticalsk8er.UHC;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import lib.PatPeter.SQLibrary.Database;
import lib.PatPeter.SQLibrary.MySQL;

import org.bukkit.ChatColor;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SqlHelper {
	private static Database sql;
	private static String gameID;
	private static String players = "0/24";
	
	public static void updateInfo(){
		System.out.println("Trying Database Connection");
		if(!sql.isOpen()){
			System.out.println("No Database Connection...");
			if(sql.open()){
				System.out.println("Sucess Connecting!");
			}
		}
		String status = "unknown";
		if(Main.GameStarted){
			status = "In Game";
		}else{
			status = "waiting";
		}
		String playercount = (Main.PlayerCount + "/24");

		try {
			ResultSet rs = sql.query("UPDATE mcgames SET title='" + gameID + "',mapname='random',status='" + status+ "',players='" + playercount+"' WHERE title='" + gameID + "'");
			//System.out.println("UPDATE mcgames SET title= " + gameID + "',mapname='random',status='" + status+ "',players='" + playercount+" WHERE title='" + gameID + "'");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}
	public static void GiveXP(Player p, Integer a){
		Damageable dam = p;
		if(dam.getHealth() <= 3){
			p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 3, 140));
		}else if(dam.getHealth() <= 6.5){
			p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 2, 140));
		}else{
			p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 1, 140));
		}
		
		
		//SqlHeper.GiveXP(p,a);
		
		p.sendMessage(ChatColor.GOLD+ "[" +ChatColor.AQUA + "PlaySuburbia" + ChatColor.GOLD+ "]" + ChatColor.DARK_GREEN + " Nice! You have earned " + ChatColor.GOLD+ a + ChatColor.DARK_GREEN + " EXP for that!");
		String user = p.getDisplayName();
		if(!sql.isOpen()){
			System.out.println("No Database Connection...");
			if(sql.open()){
				System.out.println("Sucess Connecting!");
			}
		}
		try {
			ResultSet rs = sql.query("SELECT * FROM mcusers WHERE mcname=" + "'" + p.getDisplayName()+ "'" + " LIMIT 1");
	    	System.out.println("SELECT * FROM mcusers WHERE mcname=" + "'" + p.getDisplayName()+ "'" + " LIMIT 1");
			rs.next();
	    	Integer A = rs.getInt("exp");
	    	int b = (A+a);
	    	System.out.println("Giving player " + b + "EXP");
	    	System.out.println("UPDATE mcusers SET exp='" + b + "' WHERE mcname=" + "'" + p.getDisplayName()+ "'");
			ResultSet rs2 = sql.query("UPDATE mcusers SET exp='" + b + "' WHERE mcname=" + "'" + p.getDisplayName()+ "'");
			rs2.next();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			p.sendMessage("There was a problem giving you the EXP. Please contact an admin and give them this code/make screenshot of this");
			p.sendMessage("SQLException: " + e.getLocalizedMessage());
			System.out.println("Didn't give " + p.getName() + " their " + a + " EXP points because of a error...");
			e.printStackTrace();
		}
	}
	
	public static void PreInitSQL(){
		gameID = Main.plugin.getConfig().getString("PlaySuburbia.GameID");
		if(gameID=="PLEASESETME"){
			System.out.println("PLAYSUBURIBA WARNING!");
			Main.plugin.getLogger().log(Level.SEVERE, ChatColor.RED + "PlaySuburbia.GameID is not set. REFUSING TO START!");
			Main.plugin.getLogger().log(Level.SEVERE, ChatColor.RED + "PlaySuburbia.GameID is not set. REFUSING TO START!");
			Main.plugin.getLogger().log(Level.SEVERE, ChatColor.RED + "PlaySuburbia.GameID is not set. REFUSING TO START!");
			Main.plugin.getLogger().log(Level.SEVERE, ChatColor.RED + "PlaySuburbia.GameID is not set. REFUSING TO START!");
			Main.plugin.getLogger().log(Level.SEVERE, ChatColor.RED + "PlaySuburbia Game Instance Shutting Down!");
			
			Main.plugin.getServer().shutdown();
		}else{
			Main.plugin.getLogger().log(Level.INFO, ChatColor.RED + "PlaySuburbia GameID set to " + gameID);
		}
		sql = new MySQL(Logger.getLogger("Minecraft"), 
                "[PlaySuburbia] ", 
                "91.121.2.126", 
                3306, 
                "playsuburbia", 
                "psdb", 
                "qycfn6M6j8fW77nn");
	}
	public static void SetRebooting(){
		System.out.println("Trying Database Connection");
		if(!sql.isOpen()){
			System.out.println("No Database Connection...");
			if(sql.open()){
				System.out.println("Sucess Connecting!");
			}
		}
		String status = "rebooting";
		if(Main.GameStarted){
			status = "rebooting";
		}else{
			status = "rebooting";
		}
		String playercount = (Main.PlayerCount + "/24");

		try {
			ResultSet rs = sql.query("UPDATE mcgames SET title='" + gameID + "',mapname='random',status='Restarting',players='" + playercount+"' WHERE title='" + gameID + "'");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
