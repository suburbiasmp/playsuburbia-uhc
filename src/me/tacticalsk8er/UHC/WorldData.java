package me.tacticalsk8er.UHC;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class WorldData {
	public static Double CurrentX = 1000.0;
	public static Double CurrentXn = -1000.0;
	public static Double CurrentZ = 1000.0;
	public static Double CurrentZn = -1000.0;
	public static int CurrentTime;
	public static int AddTime=1;
	public static boolean TP = false;
	/* Disable the End Round timer
	 *  
	 */
	public static boolean disableTimer = false;
	/* 
	 * 
	 */
	public static boolean directHealth = false;
	public static void StartClosing(){
		if(!disableTimer){
		int id = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
		    public void run() {
		        CurrentTime++;
		        if(AddTime!=1){
		        	CurrentTime=CurrentTime+AddTime;
		        	AddTime=1;
		        }
		        if(CurrentTime>5400){
		        	if(!TP){
		        		StringBuilder players = new StringBuilder();
		        	for (Player p : Bukkit.getServer().getOnlinePlayers()) {
		        		p.sendMessage("Teleporting to Spawn!");
		    			Location loc = ((Bukkit.getWorld("UHC").getSpawnLocation().add(0,5, 0)));
		    			Block block = loc.getBlock();
		    				players.append(p.getName() + " ");
		    				System.out.println(players);
					}
		        	Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "spreadplayers 0 0 " + "20" + " 50" + " false " + players);
		        	TP=true;
		        	}else{
		        		
		        	}
		        	
		        }else if(CurrentTime==1200){
		        	Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "[UHC]" + ChatColor.GREEN +
					("1 hour and 10 Minutes left until teleportation!"));
		        }else if(CurrentTime==2400){
		        	Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "[UHC]" + ChatColor.GREEN +"50 Minutes left until teleportation!");
		        }else if(CurrentTime==3600){
		        	Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "[UHC]" + ChatColor.GREEN +"30 minutes left until teleportation!");
		        }else if(CurrentTime==4800){
		        	Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "[UHC]" + ChatColor.GREEN +"10 minutes left until teleportation!");
		        }else if(CurrentTime==5100){
		        	Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "[UHC]" + ChatColor.GREEN +"5 minutes left until teleportation!");
		        }else if(CurrentTime==5340){
		        	Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "[UHC]" + ChatColor.GREEN +"1 minute left until teleportation!");
		        }else if(CurrentTime==1){
		        	Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "[UHC]" + ChatColor.GREEN +"1 hour and 30 minutes left until DeathMatch!");
		        }
		        if(Bukkit.getServer().getOnlinePlayers().length==0){
		        	Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "[UHC]" + ChatColor.GREEN + "Nobody Left? Let's stop!");
		        	Bukkit.getServer().shutdown();
		        }
		        
		    }
		    }, 0, 20);
		
		}else{
			Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "[UHC]" + ChatColor.GREEN + "Timer disabled.");
		}
	}
	}