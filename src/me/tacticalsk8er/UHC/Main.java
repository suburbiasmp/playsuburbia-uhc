package me.tacticalsk8er.UHC;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

public class Main extends JavaPlugin {

	public static Plugin plugin;

	public ScoreboardManager manager;
	public Scoreboard Team;
	public Team Spectators;
	
	public static boolean GameStarted = false;
	public static boolean PreGameStartCountDownStarted = false;
	public static boolean GameCountDown = false;
	public static int PlayerCount = 0;

	public void onEnable() {
		plugin = this;
		makeConfig();
		SqlHelper.PreInitSQL();
		SqlHelper.updateInfo();
		getCommand("uhc").setExecutor(new CommandUHC(this));
		this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		manager = Bukkit.getScoreboardManager();
		Team = manager.getNewScoreboard();
		Spectators = Team.registerNewTeam("Spectators");
		getServer().getPluginManager().registerEvents(new GameEvents(this, Spectators), this);

	}

	private void makeConfig() {
		getConfig().options().header();
		if (!(getConfig().contains("GameSettings.Raduis")))
			getConfig().set("GameSettings.Raduis", 1000);
		if (!(getConfig().contains("GameSettings.SpreadDistance")))
			getConfig().set("GameSettings.SpreadDistance", 100);
		if (!(getConfig().contains("TeamSettings.MaxTeams")))
			getConfig().set("TeamSettings.MaxTeams", 8);
		if (!(getConfig().contains("TeamSettings.MaxTeamSize")))
			getConfig().set("TeamSettings.MaxTeamSize", 2);
		if (!(getConfig().contains("GameSettings.EnderdragonGameEnd")))
			getConfig().set("GameSettings.EnderdragonGameEnd", true);
		if (!(getConfig().contains("PlaySuburbia.GameID")))
			getConfig().set("PlaySuburbia.GameID", "PLEASESETME");


		saveConfig();
	}
	public void onDisable(){
		
		String worldName = "world";
		File playerFilesDir = new File(worldName + "/playerdata");
		if(playerFilesDir.isDirectory()){
		String[] playerDats = playerFilesDir.list();
			for (int i = 0; i < playerDats.length; i++) {
				File datFile = new File(playerFilesDir, playerDats[i]); 
				datFile.delete();
			}
		}
		File playerFilesDir2 = new File("UHC");
		if(playerFilesDir2.isDirectory()){
		String[] playerDats = playerFilesDir2.list();
			for (int i = 0; i < playerDats.length; i++) {
				File datFile = new File(playerFilesDir2, playerDats[i]); 
				datFile.delete();
			}
		}

	}
}
