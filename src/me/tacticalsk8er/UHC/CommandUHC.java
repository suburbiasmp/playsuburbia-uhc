package me.tacticalsk8er.UHC;

import me.tacticalsk8er.UHC.Commands.CommandHelp;
import me.tacticalsk8er.UHC.Commands.CommandStart;
import me.tacticalsk8er.UHC.Commands.CommandTeam;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/*
 * This is the main class where commands will be executed and handled.
 * If you want to see what each command does, go to the commands folder.
 */

public class CommandUHC implements CommandExecutor {

	Main plugin;

	public CommandUHC(Main plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (args.length == 0) {
			new CommandHelp(sender, args);
		} else if (args.length > 0) {
			if (args[0].equalsIgnoreCase("help")) {
				new CommandHelp(sender, args);
			} else if (args[0].equalsIgnoreCase("start")) {
				new CommandStart(plugin, sender);
			} else if (args[0].equalsIgnoreCase("stop")) {

			} else if (args[0].equalsIgnoreCase("team")) {
				new CommandTeam(plugin, sender, args);
			} else if (args[0].equalsIgnoreCase("rules")) {

			}else if (args[0].equalsIgnoreCase("settime")){
				if (sender.hasPermission("uhc.shrink")){
					int am = Integer.getInteger(args[1]);
					WorldData.CurrentTime=am;
				}
			}else if (args[0].equalsIgnoreCase("time")){
				int time = WorldData.CurrentTime;
				int sLeft  = (time % 60);
				int CurrentTime = 90-((time-sLeft)/60);
				sender.sendMessage(ChatColor.GOLD+"[UHC] " +ChatColor.GREEN +CurrentTime + " minutes and " + (60-sLeft) + " seconds left until TP");			
			} else {
				sender.sendMessage("Invalid Command. Sending help prompt.");
				new CommandHelp(sender, args);
			}
		}
		return true;
	}

}
