package me.tacticalsk8er.UHC;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Random;

import me.tacticalsk8er.UHC.Commands.CommandStart;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.EnderDragon;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.scoreboard.Team;

/*
 * Ever UHC game going will use this listener to run the game, and add features such as spectating.
 * This class requires:
 * 	- Main Class (JavaPlugin)
 * 	- Game World (The world that a game is being held in)
 */

public class GameEvents implements Listener {
	public Player lastJoined;
	Main plugin;
	public static Team spectators;

	public GameEvents(Main instance, Team spectators) {
		plugin = instance;
		this.spectators = spectators;
	}

	// When player dies, player joins spectators, is invisible, and can fly.
	@EventHandler
	public void onPlayerGameDeath(final PlayerDeathEvent e) {
		if (Main.GameStarted) {
			Main.PlayerCount=Bukkit.getServer().getOnlinePlayers().length-1;
			SqlHelper.updateInfo();
			Bukkit.getServer().broadcastMessage(ChatColor.RED + e.getEntity().getName() + " died, " + Main.PlayerCount + " players left");
			if(Main.PlayerCount == 1){
				if(e.getEntity().getKiller() instanceof Player){
					SqlHelper.GiveXP(e.getEntity().getKiller(), 25);
				}
				Player p1 = null;
				if(Bukkit.getOnlinePlayers()[0]==e.getEntity()){
					Bukkit.getServer().broadcastMessage(ChatColor.GREEN + "=======================");
					Bukkit.getServer().broadcastMessage(ChatColor.DARK_GREEN + Bukkit.getOnlinePlayers()[1].getName() + ChatColor.GREEN + " Won this round of UHC" );
					Bukkit.getServer().broadcastMessage(ChatColor.GREEN + "=======================");
					Bukkit.getServer().broadcastMessage(ChatColor.GREEN + " Sending everyone to lobby in 10 seconds. please wait!");
					p1 = Bukkit.getOnlinePlayers()[1];
				}else{
					Bukkit.getServer().broadcastMessage(ChatColor.GREEN + "=======================");
					Bukkit.getServer().broadcastMessage(ChatColor.DARK_GREEN + Bukkit.getOnlinePlayers()[0].getName() + ChatColor.GREEN + " Won this round of UHC" );
					Bukkit.getServer().broadcastMessage(ChatColor.GREEN + "=======================");
					Bukkit.getServer().broadcastMessage(ChatColor.GREEN + " Sending everyone to lobby in 10 seconds. please wait!");
					p1 = Bukkit.getOnlinePlayers()[0];
				}
				final Player p = p1;
				p1.getInventory().clear();
				
				int id = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
				    public void run() {
				    	 int x = p.getLocation().getBlockX();
					        int z = p.getLocation().getBlockZ();
					       
					        Random r = new Random();
					       
					        int xr = r.nextInt(10) + 1;
					        int zr = r.nextInt(10) + 1;
					       
					        int minOrPlus = r.nextInt(2) + 1;
					       
					        int fx;
					        int fz;
					       
					        switch (minOrPlus){
					        default:
					        case 1:
					            fx = x - xr;
					            break;
					        case 2:
					            fx = x + xr;
					        }
					       
					        switch (minOrPlus){
					        default:
					        case 1:
					            fz = z - zr;
					            break;
					        case 2:
					            fz = z + zr;
					        }
					       
					        Location loc = new Location(p.getWorld(), fx, p.getLocation().getBlockY(), fz);
					       
				            //Spawn the Firework
				            Firework fw = (Firework) p.getWorld().spawnEntity(loc, EntityType.FIREWORK);
				            FireworkMeta fwm = fw.getFireworkMeta();

				            //random


				            //sets type
				            int rt = r.nextInt(5) + 1;
				            Type type = Type.BALL;
				            if (rt == 1) type = Type.BALL;
				            if (rt == 2) type = Type.BALL_LARGE;
				            if (rt == 3) type = Type.BURST;
				            if (rt == 4) type = Type.CREEPER;
				            if (rt == 5) type = Type.STAR;

				            //colors
				            //To be Added
				            int r1 = r.nextInt(256);
				            int b = r.nextInt(256);
				            int g = r.nextInt(256);
				            Color c1 = Color.fromRGB(r1, g, b);

				            r1 = r.nextInt(256);
				            b = r.nextInt(256);
				            g = r.nextInt(256);
				            Color c2 = Color.fromRGB(r1, g, b);

				            //effect
				            FireworkEffect effect = FireworkEffect.builder().flicker(r.nextBoolean()).withColor(c1).withFade(c2).with(type).trail(r.nextBoolean()).build();

				            //applied effects
				            fwm.addEffect(effect);

				            //random power! moar sulphur!
				            int rp = r.nextInt(2) + 1;
				            fwm.setPower(rp);

				            //aaaaaand set it
				            fw.setFireworkMeta(fwm);
				        
				    }
				    }, 0, 20);
				
				 e.setDeathMessage(null);
				
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					int seconds = 5;
					public void run() {
						Bukkit.getServer().broadcastMessage("Sending everyone to lobby");
						for (Player p : Bukkit.getServer().getOnlinePlayers()) {
							SendPlayerToServer(p, "lobby1");
						}
					}

				},400);


				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					int seconds = 5;

					public void run() {
						Bukkit.getServer().shutdown();
					}

				},500);

			}else{
				final Player player = e.getEntity();
				player.sendMessage("You will be sent to the lobby in 10 seconds!");
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						SendPlayerToServer(e.getEntity(),"lobby1");
					}

				},200L);
				
				SqlHelper.GiveXP(e.getEntity().getKiller(), 25);
				
			}
		}
	}

	// Player loses if player leaves
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent e) {
		if(e.getQuitMessage().contains("started")){
			return;
		}else{
			
		}
		
		
	}

	// Used for stopping player movement when count down timer is going.
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e) {
		if (Main.GameCountDown) {
			if(e.getTo().getX()!=e.getFrom().getX())
			e.getPlayer().teleport(e.getFrom());
			lastJoined = e.getPlayer();
		}

	}

	// Spectators Can't Break Blocks
	@EventHandler
	public void onSpectatorBreak(BlockBreakEvent e) {
		Player p = e.getPlayer();
		if (spectators.hasPlayer(p)) {
			e.setCancelled(true);
			p.sendMessage("You can't break blocks while spectating!");
		}else if(!(Main.GameStarted)){
			e.setCancelled(true);
		}else if(Main.GameCountDown){
			e.setCancelled(true);
		}
	}

	// Spectator Can't Hurt Players
	@EventHandler
	public void onSpectatorHurtPlayer(EntityDamageByEntityEvent e) {
		if ((e.getEntity() instanceof Player) && (e.getDamager() instanceof Player)) {
			Player damager = (Player) e.getEntity();
			if (spectators.hasPlayer(damager)) {
				e.setCancelled(true);
				damager.sendMessage("You can't hurt players while spectating!");
			}
		}
	}

	// kick player if game has started, NEEDS Checking if the player died in
	// game
	// or not (crashes on world generation, read time outs)
	@EventHandler
	public void OnLogin(PlayerJoinEvent event) {
		if (Main.GameStarted) {
			event.setJoinMessage("");
			event.getPlayer().kickPlayer("Game In Progress - Try again later");
			
		}else{
			SqlHelper.updateInfo();
			if(Bukkit.getServer().getOnlinePlayers().length == 18){
				if(!(Main.PreGameStartCountDownStarted)){
					Main.PreGameStartCountDownStarted=true;
				
				Main.plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
					int seconds = 60;
					public void run() {
					if (seconds > 0) {
						Bukkit.getServer().broadcastMessage(ChatColor.AQUA + "[UHC]" + ChatColor.RED +  seconds + ChatColor.GOLD +  "second(s) remaining!");
						for (Player p : Bukkit.getServer().getOnlinePlayers()) {
							p.playSound(p.getLocation(), Sound.NOTE_PIANO, 1f, .5f);
						}
					} else if (seconds == 0) {
						Bukkit.getServer().broadcastMessage("Game Starting!");
						new CommandStart(plugin, Bukkit.getConsoleSender());
					}else{
						
					}
				seconds--;
					}

				}, 1800L, 20L);
			}
			}
			Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "[UHC]" + ChatColor.GREEN + event.getPlayer().getName() + " joined UHC - " + ChatColor.DARK_GREEN + Bukkit.getServer().getOnlinePlayers().length+  "/24");
			if(!(Main.PreGameStartCountDownStarted)){
				Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "[UHC]" + ChatColor.GREEN + (18 -Bukkit.getServer().getOnlinePlayers().length) + " more players needed to start. Invite your" + ChatColor.BLUE + " friends!");
			}
			
		}

	}

	@EventHandler
	public void onEntityDeath(EntityDeathEvent event) {
		// see if the Enderdragon died
		Entity entity = event.getEntity();
		if (entity instanceof EnderDragon) {
			if (plugin.getConfig().getBoolean("GameSettings.EnderdragonGameEnd")) {
				if (event.getEntity().getKiller() != null) {
					Player player = event.getEntity().getKiller();
					plugin.getServer().broadcastMessage(player.getName() + ChatColor.GREEN + player.getName().toString() + "won the games by killing the EnderDragon");
				}

			}
		}

	}
	public static void SendPlayerToServer(Player p, String server){
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(b);
		
		try {
			out.writeUTF("Connect");
			out.writeUTF(server);
		} catch (IOException e) {
			e.printStackTrace();
		}
		

		Plugin plugin = Main.plugin;
		p.sendPluginMessage(plugin, "BungeeCord", b.toByteArray());
	}
	@EventHandler
	public void onPlayerDamage(EntityDamageEvent e){
		if(Main.GameCountDown){
			e.setCancelled(true);
		}
	}
}
